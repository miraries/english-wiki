# English Wiki Project

Available at Gitlab pages: [https://miraries.gitlab.io/english-wiki/](https://miraries.gitlab.io/english-wiki/)

## Info

Other than to complete the primary goal - the class project, the idea was to make use of Vuepress and Gitlab CI/CD to deploy changes to Gitlab Pages automatically on commit.