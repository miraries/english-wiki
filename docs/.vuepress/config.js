module.exports = {
    title: 'English Wiki',
    description: 'Informational website about security vulnerabilities',
    base: "/english-wiki/",
    dest: "public",
    serviceWorker: {
        updatePopup: true
    },
    evergreen: true,
    head: [
        ['link', { rel: 'icon', href: `/logo.png` }],
        ['link', { rel: 'manifest', href: '/manifest.json' }],
        ['meta', { name: 'theme-color', content: '#af3e3e' }],
        ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
        ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }],
        ['link', { rel: 'apple-touch-icon', href: `/icons/icon-152x152.png` }],
        ['meta', { name: 'msapplication-TileImage', content: '/icons/icon-144x144.png' }],
        ['meta', { name: 'msapplication-TileColor', content: '#000000' }]
    ],
    themeConfig: {
        lastUpdated: 'Last Updated',
        nav: [
            { text: 'Home', link: '/' },
            { text: 'Repo', link: 'https://gitlab.com/miraries/english-wiki' },
        ],
        sidebarDepth: 2,
        displayAllHeaders: true,
        sidebar: {
            '/': [
                    '',
                    'general',
                    'common_problems',
                    'pentesting',
                    'references'
            ]
        }
    }
}