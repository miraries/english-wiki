# Common problems in Web Apps

## SQL Injection
Databases are an essential piece in the architecture of any system that deals with data. In cases where such data contains identifiable, personal information, breach or leak of such data poses a huge issue, especially for governments, medical institutions and alike.

**Injection** occurs when the user input is sent to the system as part of command or query and trick the it into executing unintended commands and gives access to unauthorized data. Of course, several methods can be used to mitigate such attacks and they should always be implemented without compromise.

### Example
For example, for a following query:
```sql
SELECT * FROM Users WHERE UserId = " + txtUserId;
```
If the input is not sanitized the user, instead of single number (eg. 23) for the ID can input something like:
```sql
23 OR 1=1
```
So the query becomes:
```sql
SELECT UserId, Name, Password FROM Users WHERE UserId = 23 or 1=1;
```
Which means it will select the user with the ID of 23, but also every user for which the statement 1=1 is true, which is everyone!
More destructive queries can also be submitted:
```sql
105; DROP TABLE User;
```
Which would delete all users!

### Mitigations
* Data encryption instead of storing plain text password is essentially a last resort when it comes to securing user accounts
* Prepared statements render sql injections a completely useless attack surface. They essentially compile the statements before they’re executed which means whatever data comes in as an argument of the query, it cannot modify the way the query itself works.
* Access control lists, limiting data only to parts of the application can also be helpful containg a breach when it does happen

## Cross site Scripting
Cross-Site Scripting (XSS) is a vulnerability also found across web applications. Even though it's found in web applications it's actually a client side attack in which the attacker injects and runs a malicious script into a legitimate web page. It's different from the injection because the code runs on the users browser's. Browsers are capable of displaying HTML and executing JavaScript. If the application does not sanitize input and escape special characters in the input/output and reflects user input as-is back to the browser, an attacker might to execute a Cross-Site Scripting (XSS) attack successfully.

### Examples

#### Session hijacking
In this example the attacker essentially just takes the cookies stored in the browser and exports them with javascript:
```html
http://example.com/addUser?name=<script>alert(document.cookie)</script>
```
So what happens is the website will show the list of cookies like so:
![An image](./img/xss.png)

But we can also send the cookies wherever we want with an http request. We will see how this is mitigated [Mitigation](#mitigations-2)

#### Perform unauthorized activities

The example above only works in certain conditions however. In cases where we cannot extract the cookie information or send any data outside the website, we can instead act as the user on the website itself. For example, we can forge a request that changes the users e-mail or password! That would essential mean we have access to their account.

We would perform that with the following code:
```js
fetch('http://example.com/updateProfile', {email: 'attacker@evil.com'})
```

Other forms of XSS revolve around these two concepts so we will end the examples here.

### Mitigations

* As mentioned already, santize input!
* Allow untrusted data to be inserted only according to a whitelist
* Use HTTPOnly cookie flag. This basically makes cookies invisible to the client through javascript so there is no way for an attacker to extract this information in any way.
* Use a Content Security Policy (CSP) and if needed set up Cross-Origin Resource Sharing (CORS). This makes the attackers request to outside of the website impossible as only requets to certain domains are allowed. So for example the attacker cannot connect to a server he has access to and receive data.

## Failure to restrict URL Access
Using this technique, an attacker can bypass website security by accessing files directly instead of following links. This enables the attacker to access data source files directly instead of using the web application. The attacker can then guess the names of backup files that contain sensitive information, locate and read source code, or other information left on the server, and bypass the "order" of web pages.

Simply put, Failure to Restrict URL Access occurs when an error in access-control settings results in users being able to access pages that are meant to be restricted or hidden. This presents a security concern as these pages frequently are less protected than pages that are meant for public access, and unauthorized users are able to reach the pages anonymously. In many cases, the only protection used for hidden or restricted pages is not linking to the pages or not publicly showing links to them.

Attackers can use fairly simple methods to access and interact with hidden/unlinked pages on a site, the most common being a type of attack called “forced browsing". Forced browsing attacks can take place when an attacker is able to correctly guess the URL of or use brute force to access an unprotected page.

### Examples

These would be some examples of files an attacker could abuse to access unprotected data:
* `http://example.com/app/getappInfo`
* `http://example.com/app/admin-panel`
* `http://example.com/app/log_2019_06_07_2323.log`
* `http://example.com/app/install.php`
* `http://example.com/app/backup.zip`