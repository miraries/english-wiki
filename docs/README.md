---
home: true
heroImage: /hero.png
actionText: Begin →
actionLink: /general.html 
features:
- title: Good Overview
  details: This page provides a good overview of the current state of security vulenrabilities in software
- title: Proofs of concepts
  details: Not only do we explain the general concepts behind the exploits, we provide actual malicious code used in real life 
- title: Responsible disclosure
  details: One of the main albeit overlooked parts of the whole topic is how to disclose bugs once they're found
footer: MIT Licensed | Copyright © 2019-present Ivan Kotlaja
---