# Security vulnerabilities

## General info
> In computer security, a vulnerability is a weakness which can be exploited by a threat actor, such as an attacker, to perform unauthorized actions within a computer system. To exploit a vulnerability, an attacker must have at least one applicable tool or technique that can connect to a system weakness. In this frame, vulnerability is also known as the attack surface.

Not all vulnerabilities can be exploited through software, or rather code. A lot has to do with the users of the system and how educated they are on the common techniques used by attackers in the area of **social engineering**.

There are also vulnerabilities without risk. Risk actually represents the impact caused by exploitation of the vulnerability. There are several standards which define what exactly is a vulnerability but the explanation provided is enough for the time.

A **security bug** is a narrower term but is essentially the topic of this page. It only includes the bugs caused by the software, i.e. the code powering the software.

An important term, especially when talking about responsible vulnerability disclosure is a **zero-day bug**. It represents a situation where usually high impact bug is found, and it's use is detected on actual production systems. This is obviously a hugely problematic situation when it happens but luckily most bugs are handled responsibly and no proof of concepts are released until a proper fix is in place. As we'll soon see however, there are exceptions.

## Team
* Jelena Vujošević
* Ivan Kotlaja [encore.ivan@gmail.com](mailto:encore.ivan@gmail.com)
